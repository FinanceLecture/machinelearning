# Predicting Credit Ratings With Machine Learning 

# Description

The goal of the project was to estimate the long term Credit Ratings of companies listed in the S&P 500 in the year 2015. To do so fundamental data such as debt-to-asset data is used. The selected columns are selected dynamically over the selected_columns file. Upon entering a new measure, it will be considered in the programs context. Based on these features multiple machine learning algorithms are trained: Random Forest, Support Vector Classifier and Neural Networks. In the paper they are then evaluated using a confusion matrix. The results are promising, which suggests that AI-models may provide advantages in company's credit ratings



# Organization
The full project description can be found under: machinelearning/docs/abstract_group1.pdf.The data which was used for the jupyter notebook can also be found in the subfolder data. The latex subfolder includes the scripts with which we generated the abstract pdf.

All python files which were used for the project are in the src folder and the data which is used by the code lies in folder resources.
The "main" file for the source code is called creditRatingsFunctions.

The final version is tagged with the name final_version.
