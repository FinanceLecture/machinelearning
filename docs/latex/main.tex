\documentclass[a4paper, 12pt, abstracton, fleqn]{article}

%% Seitenränder, Zeilenabstand, etc.
\usepackage[a4paper, left=2.5cm, right=2.5cm, top=2.5cm]{geometry}
\usepackage[onehalfspacing]{setspace}

%% Sprache, Umlaute, etc.
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}

%% Mathematik
\usepackage{amsmath, latexsym, amssymb}
%% für Variablen definieren nach Equation (https://tex.stackexchange.com/questions/95838/how-to-write-a-perfect-equation-parameters-description)
\usepackage{array}
\newenvironment{conditions}
  {\par\vspace{\abovedisplayskip}\noindent\begin{tabular}{>{$}l<{$} @{${}={}$} l}}
  {\end{tabular}\par\vspace{\belowdisplayskip}}

%% Zitieren, Literaturverzeichnis etc.
\usepackage{apacite}
\usepackage{natbib}
% \cite{} --> Zitation in Text
% \citep{} --> Zitation in Klammer
% \cite*{}, \citep*{} --> Zitation mit allen Autoren

    
%% Grafiken, Bilder, Tabellen, etc.
\usepackage{graphicx}
\usepackage{float}
\usepackage{caption}
\captionsetup[table]{position=above, skip=0.25cm}
\usepackage{hhline}
\usepackage{hyperref}
\usepackage[export]{adjustbox}
\usepackage{subfigure}
\usepackage{tabularx}

\begin{document} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Titelseite
\newpage
\vspace*{1cm}
\begin{center}
Introduction to Machine Learning \\
(BOEC0385)
\end{center}
\vspace*{0.5cm}
\begin{center}
\thispagestyle{empty}
{\Large \bf term paper}\\[1.5cm]
{\LARGE Long Term Credit Rating Projection}\\[1.5cm]
{\bf \large Department of Banking and Finance (DBF)\\
University of Zurich}\\[1cm]
{\Large Spring Semester 2020}\\[1.5cm]
{\large Benjamin Zimmermann} \\[2.5cm]
\end{center}

\begin{table}[H]
\caption*{submitted by:}
\vspace*{-0.5cm}
\begin{center}
\begin{tabular}{ll}
Fabian Künzler			&	15-941-842	\\
Rafael Grünenfelder	    &	17-725-177	\\
Andrin Plüss			&	17-707-654  \\
Bela Koch				&	17-734-377
\end{tabular}
\end{center}
\end{table}
\newpage

%% Abstract
\pagenumbering{roman}
\selectlanguage{english}
\begin{abstract}
\noindent
The goal of this term paper is the projection of long term credit ratings on the basis of historical data of firms listed in the S\&P 500 Index using quarterly data for the observation period January 2010 to January 2017. Different statistical models and machine learning tools were applied, whereby highest performance was reached using Random Forest, Support Vector Classifier and Neural Networks. We give a brief overview on the theory of both bond ratings and the models used and describe the procedure followed.
\end{abstract}
\newpage

%% Inhaltsverzeichnis
\begingroup
\parskip=-2pt
\tableofcontents
\endgroup
\newpage

%% Abbildungs- und Tabellenverzeichnis
\addcontentsline{toc}{subsection}{List of Figures}
\listoffigures
\addcontentsline{toc}{subsection}{List of Tables}
\listoftables
\newpage

%% Beginn Hauptteil
\pagenumbering{arabic}
\setcounter{page}{1}

\section{Bond Ratings}
\subsection{Theory of Bond Ratings}
A bond credit rating is used as a measure for the long-term default risk of a debtor or a certain bond \citep{mondello2017}. The ratings are published by independent rating services such as Standard \& Poor's, Fitch Ratings Inc.\ and Moody's Investors Service. Those rating agencies assign a rating based on their estimation of creditworthiness and probability of default, where each agency uses their own systematic analysis \citep{mondello2017}. These agencies typically conduct a thorough financial analysis of a bond issuer, while also accounting for future expectations and outlook. Based on each agency's individual set of criteria, they then declare a bond rating as a letter based credit scoring scheme. \\
The subdivision of debtors and bonds into different rating classes comply with with US-American school marks A, B and C. Standard \& Poor's assign a rating of AAA to BBB- to investment grade bonds. These bond ratings refer to a low or medium credit risk. By contrast, non-investment grade bonds usually a Standard \& Poor's rating of BB+ to D. Non-investment grade bonds carry a medium to high credit risk and are therefore referred to as speculative grade. A rating of D represent a default of the debtor \citep{mondello2017}.
\subsection{Explanatory variables}
The aforementioned rating agencies list qualitative factors such as management ability, operating efficiency, financial flexibility, industry risk, accounting quality and market position as explanatory variables of the default risk of a firm. However, according to \citep{kim2005} most of these qualitative factors are likely reflected in the quantifiable and publicly available data. The literature suggests main categories of variables that influence bond ratings the most. According to \cite{bowe2014}, these categories include firm size, firm-specific financial or accounting ratios, and external macroeconomic factors.

\section{Data}
\subsection{Data Selection}
We obtain monthly long term credit ratings from the Wharton Research Data Services (WRDS) data base for all firms of the S\&P 500 Index. Our sample begins in January 2010 and ends in January 2017, totaling 7 years. \\
In addition, we build a large collection of stock-level predictive characteristics. These include 64 key figures with regard to size, profitability, leverage, asset management, liquidity, cash, value and other categories These characteristics are based on \cite{kim2005} and the WRDS data bases for monthly financial ratios and quarterly fundamental data.\\
We use a top-down approach for the features selection. To illustrate the feature importance, we implement the \textit{RandomForestClassifier} from the \textit{sklearn.ensemble} package. We gradually decrease the number of features based on the analysis of the particular features importance and the results of the implementation of the statistical models. We eventually use a set of 15 features for the analysis in the next chapters, since this seems to be a reasonable trade-off between accuracy and simplicity. We provide the details of these characteristics in \autoref{tab:firm_characteristics}.

\subsection{Data Preprocessing}
\subsubsection{Merging tables}
In a first step, we merge the aforementioned financial ratios, fundamental data and credit ratings dataframes into one spreadsheet with respect to the exact firm and date. To be specific, the algorithm first perceives the columns which are specified in the \textit{selectedColumns.txt} file and only merges them in order to increase the efficiency. For our data to be timely compliant, we convert the monthly observations of financial ratios and credit ratings into quarterly data points. This conversion assigns the mean of monthly data during a business quarter to the quarterly data.

\subsubsection{Flexibility}
We try to implement our code with maximum flexibility regarding the selected columns. In order to achieve that, we create a \textit{selectedColumns.txt} file, where the specific features can be selected. Additionally, there is the possibility to specify a ratio of e.g.\ two fundamental characteristics; our algorithm takes care of this specification, calculates the ratio and appends the resulting column to the features matrix. This flexibility enables us to quickly evaluate implementations with different input characteristics.

\subsubsection{Null values}
We define a method to cope with the NaN-values in the raw merged table. In a first step, we remove all non numeric NaN-values and disregard samples with more than 50 percent of missing values. For the remaining part, we fill up missing data using linear interpolation for pairs of maximum two missing values if the data of of the prior and posterior quarter are given. Remaining missing data points are replaced with values of the prior or posterior quarter wherever possible. When there are still NaN-values in the dataframe, we try to replace them with the company's mean for the specific feature. For the remaining missing values, we use the feature's overall mean (of all quarters and all firms) for replacement.  

\subsubsection{Preparing data}
In the last step of preprocessing the data, we clean the previously merged table. Therefore, we remove samples with missing credit ratings. Furthermore, we remove credit ratings which occur less then two times in the whole spreadsheet since this is the minimum number of elements in a class required for a statistical classification algorithm. We preserve the remaining credit ratings in order to illustrate our results, namely the confusion matrix and the classification report, in a more comprehensive manner. Moreover, we deal with NaN-values as specified in the previous subsection. Following the existing bond rating literature \citep[e.g.][]{kim2005, bowe2014}, we use the natural log of the total assets as feature for the size. In a last step, we assign the \textit{ratings} column to the response vector $y$ and the selected columns to the feature matrix $x$.

\subsection{Sample Splitting and Tuning via Validation}
\label{sample_splitting_and_tuning}
We randomly split our data into a train and test set using a ratio of 5:1 (test\_size=0.2), whereby we restricted this process such that the proportion of y-values in the test set is the same as the proportion of the y-values in our underlying data \textit{(stratify=y)}. We then standardize the features. Therefore, we fit the \textit{StandardScaler} with the train set. Both the train and test data are transformed subsequently.\\
We implement different models to project the long term credit ratings. To achieve the highest accuracy possible, we create a Pipeline object for each model. We use \textit{GridSearchCV} from the \textit{sklearn.model\_selection} package to exhaustively tune the hyperparameters. Through multiple trial and error brute-force grid search runs, we find the optimal hyperparameters for each statistical model.

\subsection{Performance}
Wherever possible, our code is both memory and computationally efficient. We always pay attention that computations are done inplace and not by assigning it to a new variable. Furthermore, as previously mentioned, we only merge the necessary columns from the three raw dataframes. In addition, the \textit{merge\_tables} function has an attribute called \textit{execute} which can be set to \textit{False} in order to yield better performance when rerunning the notebook. This acceleration is only possible if the \textit{selectedColumns} have not been changed. 

\subsection{Difficulties}
We encountered several difficulties throughout the process. Firstly, the conversion of the monthly credit ratings and financial ratios to quarterly data took a lot of programming effort. Secondly, we had to find a way to deal with the several NaN-values. We therefore found a comprehensive approach for coping with NaN-values in the data cleaning process. Another issue was the evaluation of the models output. We decided to plot confusion matrices with and without normalization. Furthermore, we list the precision, recall and $\text{F}_1$-score using the \textit{metrics.classification\_report} from sklearn. Additionally, we plot the features importance for the \textit{RandomForestClassifier} (see \autoref{RF_feature_importance}).

\section{Random Forest}

\subsection{Theory of Random Forest}
Random forest is based on decision trees, which iteratively divide the predictor space into distinct and non-overlapping regions. The different observations are classified depending on the region they fall into \citep{james2013}. Applying decision trees may lead to two substantial problems. The resulting trees are often too complex and are therefore likely to overfit the data. Cross validation cannot overcome this problem, because each fold in the training set will lead to different decision trees, which still suffer from overfitting the data. In addition, there is high variance between the different trees. A first step to address this problem is repeatedly training the model with samples from the data set and averaging all the predictions to reduce the variance but keeping the low bias provided by the complexity of the individual trees \citep{james2013}. But most of the individual trees will use the same strong predictor in the top split, leading to high correlation between them. Consequently, averaging the trees will not lead to a substantial reduction in variance. The concept of random forest restricts the potential predictors on each split of the tree on a random sample chosen from the full set of predictors. As a result, many splits will not consider the strong predictors, leading to lower correlation between the individual trees and therefore reducing the variance and resulting in more reliability \citep{james2013}.


%\subsection{Implementation}
%Kommentar zu SimpleImputer? For measuring the quality of a split, we used the Gini impurity as criteria, which indicates that a given node consists primarily of observations from a single class \cite{james2013}.


\subsection{Results}
\label{RF_results}
With the optimal hyperparameters determined by the procedure described in \autoref{sample_splitting_and_tuning} we are able to correctly classify 90.39\% of the test set using random forest classifier. \autoref{RF_CM_normalized} visualizes our results in a normalized confusion matrix, showing that most classifications are correct and that there are only small deviations in ratings when the prediction is incorrect. We can neither detect high variance nor bias in our results. For the lower ratings, especially for CCC+, we have ascertained declining performance, what we attribute to smaller occurrences of low ratings in the data.  To evaluate the accuracy of our predictions we use the $\text{F}_1$-score, which corresponds to the balanced mean of precision and recall \citep{sokolova2006}. Our model yields a $\text{F}_1$-score of 0.9038 with both high precision and recall.

\begin{figure}[H]
\begin{center}
\caption{Normalized Confusion Matrix (Random Forest)}
\includegraphics[scale=0.375]{RF_CM_normalized.png}
\label{RF_CM_normalized}
\end{center}
\end{figure}

\vspace{-1.5cm}

\section{Logistic Regression}

\subsection{Theory of Logistic Regression}
\label{theory_log_reg}
Building up of the concept of linear regression, logistic regression is primary used for estimating probabilities. The use of logistic instead of linear regression for estimating probabilities fixes two shortcomings. First, when using linear regression, the results are unbounded, wherefore the resulting probabilities can be smaller than zero or can exceed one \citep{stockwatson}. Second, the linear regression approach assumes constant marginal effects of the features on the response, which is, depending on the problem, often counterintuitive \citep{stockwatson}. To correct these problems, the logistic regression approach transforms the linear input as in linear regression into a S-shaped form, bounded in the interval $[0, 1]$, using the logistic function \citep{james2013}:
\begin{equation}
\label{eq_log_reg}
    Pr(Y = y \mid X) = \frac{e^{\beta_0 + \sum_{j=1}^n \beta_j X_j}}{1 + e^{\beta_0 + \sum_{j=1}^n \beta_j X_j}}
\end{equation}
\noindent
Given \autoref{eq_log_reg}, we can classify a value $Y$ into class $y$ if the estimated probability that $Y=y$ is above a certain threshold, typically 0.5 as we used in our implementation.

\subsection{Results}
With a test score of 0.3328 and a $\text{F}_1$-score of 0.3169 logistic regression yields relatively bad performance for our classification problem compared to the other statistical models used. We can see noticeable higher variance in \autoref{LR_CM_normalized}. We assume that logistic regression performs poorly because we classify the ratings over 17 groups, while logistic regression was originally developed for binary classification.

\begin{figure}[H]
\begin{center}
\caption{Normalized Confusion Matrix (Logistic Regression)}
\includegraphics[scale=0.375]{LR_CM_normalized.png}
\label{LR_CM_normalized}
\end{center}
\end{figure}

\vspace{-1.5cm}
\newpage
\section{Support Vector Classifier}

\subsection{Theory of Support Vector Classifier}
Support vectors try to classify data by drawing hyperplanes of the form $\beta_0 + \sum_{j=1}^n \beta_j x_j = 0$ that discriminate between the different classes. This hyperplane separates each observation $y_i$ in one of two classes $\{ -1, 1 \}$ using following condition \citep{james2013}:
\begin{equation}
    y_i =   \begin{cases}
            1   & \text{if} \; \; \; \beta_0 + \sum_{j=1}^n \beta_j x_j > 0 \\
            -1  & \text{if} \; \; \; \beta_0 + \sum_{j=1}^n \beta_j x_j < 0
            \end{cases}
\end{equation}
In case of perfectly separable data, there will be an infinite number of possible hyperplanes. The concept of the maximal margin classifier maximizes the distance to the test observations, helping to decide which hyperplane to use. However, this concept cannot be applied directly when the data is not perfectly separable. Furthermore, the maximal margin hyperplane is very sensitive to individual observations, which leads to a high risk of overfitting. To address this problem, the support vector classifier allows a limited number of training observations to be on the incorrect side of the margin and even of the hyperplane, therefore increasing robustness to individual observations and better classification of most of the training observations \citep{james2013}. 

\subsection{Results}
Using the optimal hyperparameters, the support vector classifier reaches a score of 82.27\% correctly classified ratings. With a $\text{F}_1$-score of 0.8219, precision and recall of our projection are high. As we can see in \autoref{SVC_CM_normalized}, most classifications are correct and both variance and bias is low. Reference can be made to \autoref{RF_results} for the justification. For the lower ratings, we detect small bias with a tendency of predicting the ratings too high.

\begin{figure}[H]
\begin{center}
\caption{Normalized Confusion Matrix (Support Vector Classifier)}
\includegraphics[scale=0.375]{RF_CM_normalized.png}
\label{SVC_CM_normalized}
\end{center}
\end{figure}

\vspace{-1.5cm}

\section{Neural Networks}

\subsection{Theory of Neural Networks}
The neural network used complies with the traditional feed-forward type of neural network, whereby it is implemented consisting of an input layer of raw predictors, four hidden layers and an output layer. The different hidden layers are composed of \textit{neurons}, which draw information linearly from all given inputs. These inputs are amplified or attenuated given a parameter vector including an intercept and one weight parameter per predictor \citep{gu2019}. The drawn information is transformed through the non-linear rectified linear unit, which transforms $x$ to zero if $x<0$ and maintains the same value otherwise. In a final step, the results of each \textit{neuron} are linearly aggregated into an ultimate output forecast.

\subsection{Results}
Using the neural network described above, we decided to use 25 epochs. An epoch defines how many times the model should be tested with different splits. This decision is based on experience to achieve the best trade-off between performance and accuracy, visualized in \autoref{NN_test_and_training_accuracy_history}.

\begin{figure}[H]
\begin{center}
\caption{Test and Training Accuracy History}
\includegraphics[scale=0.375]{NN_test_and_training_accuracy_history.png}
\label{NN_test_and_training_accuracy_history}
\end{center}
\end{figure}

\vspace{-0.5cm}

\noindent
The neural network achieves a score of 84.14\% with a relatively high $\text{F}_1$-score of 0.8421. As we can see in \autoref{NN_CM_normalized}, the results seems to neither have high variance nor beeing biased.

\begin{figure}[H]
\begin{center}
\caption{Normalized Confusion Matrix (Neural Networks)}
\includegraphics[scale=0.375]{NN_CM_normalized.png}
\label{NN_CM_normalized}
\end{center}
\end{figure}

\vspace{-1.5cm}
\newpage
\section{Autoregressive Model}
We also considered to include lags on the credit ratings in our model but despite the predominantly positive key statistics computed in the “linearAR” model, we ultimately concluded that lags are unable to significantly improve our forecast. The main reason for this assessment is that the panel structure of the data at hand, fails to provide enough observations for a reliable forecast with conventional time series models such as AR, MA and ARIMA. The plot in the “linearAR” function illustrates this point graphically. To circumvent this problem, we tried to employ a vector autoregressive model (VAR) to exploit the cross-sectional structure of our data. However, during the implementation we encountered several technical difficulties. At the time of writing the official documentation for “statsmodel.tsa.stattols” (the library where pythons time series routines are implemented) was unavailable. In addition, we found the Vector autoregressive model to have a considerable detrimental effect on the performance, suggesting that other programming languages such as MATLAB would be better equipped for handling time series analysis. Under the above-mentioned circumstances, we ultimately refrained from including the VAR model leaving it as an idea for further improvement.  


\section{Discussion}
Using a different approach to select the explanatory variables could possibly change the performance of our models in both directions. While random forest, support vector classifier and neural network seems to perform well, the results of logistic regression are not satisfactory. To project long term credit ratings using a logistic regression approach, replacing the sigmoid function used by logistic regression as described in \autoref{theory_log_reg} with the softmax function could yield better performance. This approach is known as multinomial logistic regression and is used when multiple classes are considered. \\
Analysing the performance of the implemented models for other indices than the S\&P 500 could provide valuable insights on both model performance and different characteristics of bond rating processes. Furthermore, it would be economically interesting to implement an algorithm which classifies investmentgrade and non-investmentgrade bonds. We think further analysis in this direction could be enriching.


%% Literaturverzeichnis
\newpage
\addcontentsline{toc}{section}{Bibliography}
\bibliographystyle{apacite}
\bibliography{bibliography_IML.bib}


%% Appendix
\newpage
\appendix
\setcounter{table}{0}
\renewcommand{\thetable}{A\arabic{table}}
\setcounter{figure}{0}
\renewcommand{\thefigure}{A\arabic{figure}}
\section{Appendix}

\subsection{Selected Columns}
\begin{table}[H]
    \centering
    \caption{Listing of firm characteristics used in the term paper}
    \scalebox{0.85}{
    \begin{tabular}{ll}
    \hline
    \textbf{Acronym} & \textbf{Firm characteristic}                          \\ \hhline{==}
    ATQ              & Size calculated as the natural log of total assets    \\ \hline
    cash\_conversion & Cash conversion cycle in days                         \\ \hline
    cash\_lt         & Cash balance divided by total assets                  \\ \hline
    debt\_assets     & Total debt divided by total assets                    \\ \hline
    debt\_capital    & Total debt divided by capital                         \\ \hline
    debt\_ebitda     & Total debt divided by EBITDA                          \\ \hline
    dpr              & Dividend payout ratio                                 \\ \hline
    gpm              & Gross profit margin                                   \\ \hline
    int\_totdebt     & Interest divided by average total debt                \\ \hline
    intcov           & Interest coverage ratio - after tax~                  \\ \hline
    intcov\_ratio    & Interest coverage ratio                               \\ \hline
    lt\_debt         & Long term debt divided by total liabilities           \\ \hline
    MV\_BV\_ratio    & Market value divided by book value                    \\ \hline
    RE\_TA\_ratio    & Retrained earnings divided by total assets            \\ \hline
    roce             & Return on capital employed                            \\ \hhline{==}
    \multicolumn{2}{l}{\textit{The data is obtained from the WRDS data base.}}
    \end{tabular}
    }
    \label{tab:firm_characteristics}
\end{table}

\subsection{Feature Importance (RandomForestClassifier)}
\begin{figure}[H]
\begin{center}
\caption{Illustration of Feature Importance for RandomForestClassifier}
\includegraphics[scale=0.375]{RF_feature_importance.png}
\label{RF_feature_importance}
\end{center}
\end{figure}


%% Eigenständigkeitserklärung
%% https://www.business.uzh.ch/dam/jcr:4aa70185-19c0-45c5-a566-520684cf8e03/Guidelines%20for%20writing%20your%20thesis_Chair%20Prof.%20Natter_03Oct2019.pdf
\newpage
\section*{Statutory Declaration}
We hereby declare that this thesis has been composed by ourselves autonomously and that no means other than those declared were used. In every single case, we have indicated parts that were taken out of published or unpublished work, either verbatim or in a paraphrased manner, as such through a quotation. This thesis has not been handed in or published before in the same or similar form.

\vspace*{2cm}

\begin{tabularx}{\textwidth}{XXXr}
Zürich, 18.04.2020 &   & \hrulefill            & \\
                &   & Fabian Künzler        & \\
&&&\\
&&&\\
Balzers, 18.04.2020 &   & \hrulefill        & \\
                &   & Rafael Grünenfelder   & \\
&&&\\
&&&\\
Rombach, 18.04.2020  &   & \hrulefill            & \\
                &   & Andrin Plüss          & \\
&&&\\
&&&\\
Solothurn, 18.04.2020  &   & \hrulefill      & \\
                &   & Bela Koch             &
\end{tabularx}

\end{document}