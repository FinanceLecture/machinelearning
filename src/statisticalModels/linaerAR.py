# Imports
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns;

sns.set(color_codes=True)
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_acf
from statsmodels.tsa.stattools import acf

#enables unlimited display for df.head()
pd.set_option('display.max_rows', None)
pd.set_option('expand_frame_repr', False)


"""
-------------------------------------------------------------------------------------------
linear_AR: Calculates key-statistics regarding the lags on the outcome variables, including:
-> ADF test
-> Autocorellation
-> plots the mean credit rating as a function of time
-> plots the autocorrelation with 5% confidence interval

--------------------------------------------------------------------------------------------
"""

def linear_AR(x, y,execute=False):
    # this function displays two graphs.
    # to display both pyCharm needs to be switched to scientific mode
    # This can be achieved by selecting scientific mode in View (not supported in PyCharm Community
    if execute==False:
        return
    df=pd.DataFrame(x)
    df["ratings"]=y

    #set date as datetime index
    df["date"] = pd.to_datetime(df['date'])
    df=df[["date","ratings"]]
    df = df.set_index('date')

    #calculate 10 days rolling mean
    df = df.sort_index()
    df["ratings"] = df.rolling('30D',min_periods=1).mean()
    df_grouped= df.groupby(pd.Grouper(level='date', freq='90D'))["ratings"].mean()
    df_grouped=pd.DataFrame(df_grouped)
    df_grouped=df_grouped.dropna()

    #Plots 90 days rolling mean as time series. We use the mean since the credit rating of firm A at time T-1 does not contain
    #any ideosyncratic information for Firm B at Time T. That is, with regular time series models we can not use the lags on the
    #credit rating of all the other firms to predict the credit rating of a particular company. The best regular AR can do is
    #predict the average credit rating based on the average of previous quarters. Since the credit ratings are done quarterly
    #we have only 27 observations available. In our opinion not enough for a reliable forecast.
    plt.figure(figsize=(15, 5))
    plt.plot(df_grouped.index, df_grouped["ratings"])
    plt.show()

    #The augmented dickey fueller test conducted below just fails to reject the hypothesis
    #of 'ratings' beeing non-stationary at the 5% level.However as the credit rating is capped,
    #the series can be assumed stationary.
    result = adfuller(df_grouped["ratings"])
    print('ADF Statistic: {}'.format(result))
    print(df['ratings'].autocorr(lag=2))

    #Calculates autocorrelation functon of the credit rating, to evaluate the predictive power of the lag component.
    #The result suggest that four of the lags have significant predictive power.
    plt2=plot_acf(df_grouped["ratings"],lags=10,alpha=0.5)
    plt2.show()

    autocorelation_function=acf(df_grouped["ratings"])
    for i in range(len(autocorelation_function)):
        print(f" {i+1}) {abs(round(float(format(autocorelation_function[i])),4))}")

