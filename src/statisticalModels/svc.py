# Imports
from sklearn.impute import SimpleImputer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC


def svc(x, y):
    # removes the column date because it is not a numerical column
    x = x.drop(columns={'date'})

    X_train, X_test, y_train, y_test = train_test_split(x, y,
                                                        test_size=0.2,
                                                        random_state=0,
                                                        stratify=y)

    # Create pipeline object with standard scaler and SVC estimator
    pipe = Pipeline([('imputer', SimpleImputer()),
                     ('scaler', StandardScaler()),
                     ('classifier', LogisticRegression())])

    # Define parameter grid
    param_grid = [{'imputer': [SimpleImputer(strategy='mean')],
                   'scaler': [StandardScaler()],
                   'classifier': [LogisticRegression(max_iter=1000000)],
                   'classifier__C': [1, 100, 1000]},
                  {'imputer': [SimpleImputer(strategy='mean')],
                   'scaler': [StandardScaler()],
                   'classifier': [SVC(kernel='rbf')],
                   'classifier__gamma': [1, 10],
                   'classifier__C': [10, 100]}]

    # Run grid search
    gs = GridSearchCV(pipe, param_grid, cv=5, n_jobs=-1)
    gs.fit(X_train, y_train)

    best_score = gs.best_score_
    score = gs.score(X_test, y_test)
    params = gs.best_params_

    print('Best CV accuracy: {:.2f}'.format(best_score))
    print('Test score:       {:.2f}'.format(score))
    print('Best parameters: {}'.format(params))

    return [best_score, score, params]