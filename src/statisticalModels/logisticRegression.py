# Imports
from sklearn.impute import KNNImputer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler

def logistic_regression(x, y):
    # removes the column date because it is not a numerical column
    x = x.drop(columns={'date'})
    pipe = Pipeline([('imputer', KNNImputer()),
                     ('scaler', StandardScaler()),
                     ('classifier', LogisticRegression())])

    # Define the hyperparameter values to be tested
    param_grid = [{'imputer': [KNNImputer(n_neighbors=5)],
                   'scaler': [StandardScaler()],
                   'classifier': [LogisticRegression(max_iter=1000000)],
                   'classifier__C': [1, 100, 1000]}]

    # Run brute-force grid search
    gs = GridSearchCV(estimator=pipe,
                      param_grid=param_grid,
                      scoring='accuracy',
                      cv=5, n_jobs=-1)
    gs = gs.fit(x, y)

    best_score = gs.best_score_
    score = gs.score(x, y)
    params = gs.best_params_

    print('Best CV accuracy: {:.2f}'.format(best_score))
    print('Test score:       {:.2f}'.format(score))
    print('Best parameters: {}'.format(params))

    return [best_score, score, params]