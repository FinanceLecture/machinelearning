# Imports
from sklearn.ensemble import RandomForestClassifier
from sklearn.impute import SimpleImputer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import train_test_split

import numpy as np
import matplotlib.pyplot as plt
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler


def random_forest(x, y, plot=False):
    # removes the column date because it is not a numerical column

    x = x.drop(columns={'date'})
    X_train, X_test, y_train, y_test = train_test_split(x, y,
                                                        test_size=0.2,
                                                        random_state=0,
                                                        stratify=y)

    # Create Pipeline object with standard scaler and RandomForest estimator
    pipe = Pipeline([('imputer', SimpleImputer()),
                     ('scaler', StandardScaler()),
                     ('classifier', RandomForestClassifier())])

    param_grid = [{'imputer': [SimpleImputer(strategy='mean')],
                   'scaler': [StandardScaler()],
                   'classifier': [RandomForestClassifier(criterion='gini')],
                   'classifier__max_depth': [1, 10, 100, None],
                   'classifier__min_samples_split': [2, 10, 20]}]
    # Define the hyperparameter values to be tested

    # Run brute-force grid search
    gs = GridSearchCV(estimator=pipe,
                      param_grid=param_grid,
                      scoring='accuracy',
                      cv=5, n_jobs=-1)
    gs = gs.fit(X_train, y_train)

    best_score = gs.best_score_
    score = gs.score(X_test, y_test)
    params = gs.best_params_

    print('Best CV accuracy: {:.2f}'.format(best_score))
    print('Test score:       {:.2f}'.format(score))
    print('Best parameters: {}'.format(params))

    # display plot of feature importance if plot==True
    if plot == True:

        # Get list of feature labels
        feat_labels = x.columns

        # fit the training data and extract feature importance attributes
        forest = RandomForestClassifier(random_state=1)
        forest.fit(X_train, y_train)

        importances = forest.feature_importances_

        # print all features in order of their importance. The scores add up to one
        indices = np.argsort(importances)[::-1]
        n = len(indices)
        print("\n\n\n")
        print(" parameter name | relative importance")
        print(" ------------------------------------")
        for i in range(n):
            print('{0:2d}) {1:26s} {2:6.4f}'.format(i + 1,
                                                    feat_labels[indices[i] - 1],
                                                    importances[indices[i]]))
        feat_imp = np.sort(importances)[::-1]
        sum_feat_imp = np.cumsum(feat_imp)[:n]

        # todo: Add hovertool
        plt.figure(figsize=(12, 8))
        plt.bar(range(n), importances[indices[:n]], align='center')
        plt.xticks(range(n), feat_labels[indices[:n]], rotation=90)
        plt.xlim([-1, n])
        plt.xlabel('Feature')
        plt.ylabel('Rel. Feature Importance')
        plt.step(range(n), sum_feat_imp, where='mid',
                 label='Cumulative importance')
        plt.tight_layout()
        plt.show()

    return [best_score, score, params]
