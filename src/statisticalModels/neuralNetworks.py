#Tensorflow is the library for neural networks
#In the last weeks there was an update
#if you are running this on a windows machine you might download DLL to work
#You can find the newest one on the official website of microsoft here:
#https://support.microsoft.com/de-de/help/2977003/the-latest-supported-visual-c-downloads
import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

def neural_networks(x,y):
    # removes the column date because it is not a numerical column
    x=x.drop(columns={'date'})

    # splits the data into train (80%) and test set (20%)
    X_train, X_test, y_train, y_test = train_test_split(x, y,
                                                        test_size=0.2,
                                                        random_state=0,
                                                        stratify=y)
    # transform all of them into dataframes/
    # make sure all of them are dataframes
    X_train = pd.DataFrame(X_train)
    y_train = pd.DataFrame(y_train)
    X_test = pd.DataFrame(X_test)
    y_test = pd.DataFrame(y_test)

    # create the input data for the neural networ
    # basically create nodes for each data by removing the array in the arrays.
    # defining the batch size for train set with 25 => results in best performance/accuracy results based on our trials
    train_dataset = tf.data.Dataset.from_tensor_slices((X_train.values, y_train.values)).batch(25)
    test_dataset = tf.data.Dataset.from_tensor_slices((X_test.values, y_test.values)).batch(25)

    # creating the model with several layers in between which allows the model to develop patterns
    # these were constructed through trial and error => by looking what the best accuracy is
    model = tf.keras.Sequential([
        tf.keras.layers.Dense(1216, activation='relu'),
        tf.keras.layers.Dense(608, activation='relu'),
        tf.keras.layers.Dense(304, activation='relu'),
        tf.keras.layers.Dense(121, activation='relu'),
        # the percentages should add up to one (softmax)
        tf.keras.layers.Dense(19, activation='softmax'),
    ])

    # compiling the model
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    # using 20 epochs based on experience which gives best performance/accuracy compromise
    # An epoch basically defines how many times the model should be tested with different splits of the train-split
    model.fit(train_dataset, epochs=20, shuffle=True)
    test_loss,test_acc=model.evaluate(test_dataset)

    print(f"test accuracy: {test_acc}")

    return test_acc