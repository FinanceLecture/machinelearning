#---------------------
# Imports of panda, numpy and matplotlib package
import matplotlib.pyplot as plt
import pandas as pd

from src.statisticalModels.linaerAR import linear_AR

plt.style.use('seaborn-whitegrid')
plt.rcParams['font.size'] = 14
# imports the merge_tables function and the path and file name of the file in which the merged table will be stored
# os stands for operating system and is needed to later read the relative path
from src.createCombinedTab.combineTabels import merge_tables, relative_path_combined, file_ratings_quaterly__, os

# imports the prepare_data function
from src.prepareData.prepareData import prepare_data

# imports the implemented statistcal models
from src.statisticalModels.randomForest import random_forest
from src.statisticalModels.logisticRegression import logistic_regression
from src.statisticalModels.svc import svc
from src.statisticalModels.neuralNetworks import neural_networks
#---------------------

# This file is the main file of the application. To test and execute this application this file has to be run
# If some models should't be executed then they need to be commented out

# This variable should store all dates which should be parsed as dates when read from the combined table
# For each case in this application only should be parsed as date because it's the only one needed for further
# computation which need a date type
__all_parameter_dates__ = ['date']

# This function combines all dataframes together and writes them to the quaterlyRatings.csv
# It only takes a boolean as input parameter. The default is True
# If we haven't changed the selected columns we don't need to merge the table again
# and therefore achieve a higher performance
merge_tables(execute=True)

# to display the whole dataframe in a print statement
# pd.set_option('display.max_rows', None)
# pd.set_option('display.max_columns', None)
# pd.set_option('display.width', None)

# read the data of the joined tables from the "result file"
df = pd.read_csv(os.path.join(relative_path_combined,file_ratings_quaterly__), sep=',', parse_dates=__all_parameter_dates__)

# clean null values and prepare data for the statistical models
(x, y) = prepare_data(df)

# evaluation of neural networks model
# expected time 1min, best test accuracy 56% (see pdf)
neural_networks(x, y)

# evaluation of logistic regression model
# expected time 3min, best test accuracy 56% (see pdf)
logistic_regression(x, y)

# evaluation of random forest model
# expected time 1min (without plot), best test accuracy 93% (see pdf)
random_forest(x,y, plot=False)

# evaluation of svc model
# expected time 4min, best test accuracy 66% (see pdf)
svc(x,y)

#linear_AR(x, y,execute=True)


print("finished program")
