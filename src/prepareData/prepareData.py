import os

import numpy as np
import pandas as pd
from sklearn import preprocessing as pp

from src.Exceptions.ExceptionHandler import empty_dataframe
from src.createCombinedTab.combineTabels import __file_selected_colums, relative_path_economic_data

relative_path_col_list = os.path.realpath('../resources/economicData')
__file_name_tic__ = 'tickerList'


def create_shifted_data(df, shifted_data):
    # get a list of all column names
    columns = list(df.columns)

    # remove all columns which shouldn't be shifted especially the y vector (ratings)
    columns.remove("date")
    columns.remove("tic")
    columns.remove("gvkey")
    columns.remove("ratings")

    # print(df.head(60))
    # group the dataframe by the primary key (tic) and shift the specified columns
    df[columns] = df.groupby('tic').apply(lambda group: group[columns].shift(shifted_data))
    # afterwards we need to delete the number of leftover rows
    # since we don't want the tic to be the new index (complicates further calculation) we set as_index=False
    df = df.groupby(['tic'], as_index=False).apply(lambda group: group.iloc[shifted_data:])
    #print(df.head(60))
    return df


def remove_nan_ratings(df):
    nan_value = float("NaN")
    # replace the empty string with nan values
    # such that all non existing ratings can be dropped
    df.replace("", nan_value, inplace=True)
    # drop all nan values
    df.dropna(subset=["ratings"], inplace=True)
    return df


def remove_tic(df):
    # if there is a tic it removes it from the dataframe
    try:
        df = df.drop(columns=["tic"])
    finally:
        return df


def check_min_occurrence(df, min_occ_rating):
    # factorize temporarily to be able to use np.bincount and to use the index functionality of that
    y = pd.factorize(df['ratings'])

    # list of ratings which appear to little
    list_of_ratings_to_remove = []

    # go over the appearances (np.bincount) of the single ratings and check
    # if a rating appears enough. If not the rating is selected over the index and appended to the array
    for index, elem in enumerate(np.bincount(y[0])):
        if elem < (min_occ_rating if min_occ_rating > 2 else 3):
            list_of_ratings_to_remove.append(y[1].values[index])

    # if the array is not empty all ratings which are in the list are removed from the dataframe
    if (len(list_of_ratings_to_remove) > 0):
        df = df[~df['ratings'].isin(list_of_ratings_to_remove)]
    return df


def factorize(df, array):
    # this function creates numerical values out of non numerical values

    # we decided to go with a labelencoder since it is simple to use and does the job
    # even though it is not meant to be used with more than two categories
    le = pp.LabelEncoder()

    # every non numerical column (except date) will get transformed
    for column in array:
        if not column == "date":
            s = list(pd.Series(le.fit_transform(df[column])))
            df[column] = s

    # returns a purely numerical df (except date)
    return df


def clean_data(df, min_occ_rating, non_numeric_to_numeric, shift_data):
    # if a shift is specified the data is transformed to have lagged data in the end
    # if none is specified (default) this step will be skipped
    if shift_data is not None: df = create_shifted_data(df, shift_data)

    # remove all ratings which are nan
    df = remove_nan_ratings(df)

    # when no array of non numeric is specified the program has to determine them by itself
    if non_numeric_to_numeric is None:
        # create a real copy (a df which has a not a reference to the same data object
        df_copy = df.copy()

        try:
            # exclude all acceptable columns (int, float) from the copy and get the column names
            # wrapped in a try and catch since the df can be empty
            non_numeric_to_numeric = list(df_copy.select_dtypes(exclude=["float64", "int"]).columns)
        except:
            # in case of an error the custom error is raised
            empty_dataframe()

    # transform non numerical into numerical values
    df = factorize(df, non_numeric_to_numeric)

    # remove date temporarily, because the following transformation require all values to be numerical
    # only one value which is not numerical (date), which is needed later for testing
    date = df.pop("date")

    # null values in the x matrix are manipulated
    df = deal_with_nan(df)

    # make sure that ratings appear at least x times according to the min_occ_rating variable
    df = check_min_occurrence(df, min_occ_rating)

    # delete the tic from the dataframe since the rating should not be determined by the primary key
    df = remove_tic(df)

    # append the before removed date again
    df['date'] = date
    return df


def get_selected_colums():
    # functions retrieves all column names from the selected_columns file
    selected_colums = []

    with open(os.path.join(relative_path_economic_data, __file_selected_colums)) as f:
        while True:
            # reads the selected_columns file line by line
            line = f.readline()

            # if the last line is reached it stops the loop
            if not line:
                break

            # to remove the line break
            line = line.rstrip()

            # checks if the line contains a calculation
            if "=" in line:
                # cleans the line and removes blank spaces
                line = line.replace(" ", "")
                lines = line.split("=")
                # if the result name isn't already in the array it adds it
                selected_colums = check_if_in_list(selected_colums, lines[0])
            else:
                # if no calculation is detected it just adds the column name to the array
                # if the array doesn't already include the column name
                selected_colums = check_if_in_list(selected_colums, line)

        return selected_colums


def check_if_in_list(list, value):
    # checks if a value is in an array and if is the tic column
    # if both answered with no the value is added to the list (array)
    if not value in list and not value == "tic":
        list.append(value)
    return list


def deal_with_nan(df, percentage=0.5):
    # remove all non numeric nan values (-1) and value them as nan
    df[df < 0] = None
    # if there are more then 50% of values in a row missing we drop the row
    # The ratings are still part of the df. But we only want to reduce the x matrix (everything except the rating),
    # therefore we substract 1 from the length of columns because the length of the dataframe columns consists of:
    # column ratings + columns for x matrix. Thats wy we reduce the length by one
    df = df[df.isnull().sum(axis=1) < percentage * (len(df.columns) - 1)]
    # interpolate data (if only two data points are missing between two we can interpolate them
    df = df.groupby('tic').apply(lambda group: group.interpolate(method='linear', limit=2))
    # if less then 50% are missing we try to replace with the value before or after this row
    df = df.groupby('tic').apply(lambda group: group.fillna(method='ffill', limit=2))
    df = df.groupby('tic').apply(lambda group: group.fillna(method='bfill', limit=2))
    # if the value is still nan we will try to replace it with the mean of this company
    # whenever possible we used inplace to change the data frame in the stored place and to not assign it
    # to a new variable every time we change something
    df.fillna(df.groupby('tic').transform('mean'), inplace=True)
    # if there are still nan values they are replaced with the mean of the column
    df = df.fillna(df.mean()).clip(-1e11, 1e11)
    return df


def prepare_data(df, selected_colums=get_selected_colums(), min_occ_rating=2, non_numeric_to_numeric=None,
                 shift_data=None):
    # this function takes 5 input arguments
    # the first is the dataframe which should be prepared
    # the second one is an array of column names which should be used to build the x matrix.
    # The default are all columns except tic (date and rating will be splitted later in the process)
    # date is kept because it is needed for testing later (example dickey fuller test)
    # the third one is the min_occ_rating which is passed on to another function later
    # and determines how many times a rating should at least occur such that it is included in the y vector
    # The default is 2 since this is required by sklearn package
    # The fourth is the non_numeric_to_numeric parameter which is an array which should include all column names
    # of columns which should be factorized. This is to make achieve better performance since otherwise the program
    # needs to determine itself which columns include non numerical types
    # the default is none which means later on the program finds and converts non numeric types by itself
    # the fifth is the shift_data parameter. It determines how many business quaters the data should be shifted.
    # So to achieve lagged data we basically shift the x matrix
    # The default is none which means the data of a business quater is used to determine the rating of the same quater

    # prepare the data
    df = clean_data(df, min_occ_rating, non_numeric_to_numeric, shift_data)

    # select the x matrix (without the rating) which is in the first position
    x = df[selected_colums].iloc[:, :]
    #print(x.head())

    # creates the y vector by selecting the rating
    y = df['ratings']

    # returns a tuple with the response vector in y
    # and the feature matrix x
    return (x, y)
