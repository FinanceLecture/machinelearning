def empty_dataframe():
    raise Exception('Dataframe needs to have at least the rating and one column of economic data')

def invalid_calculation(para1, para2):
    raise Exception('A calculation with parameters is invalid: %s, %s'% para1, para2)

def invalid_calculation():
    raise Exception('A calculation is invalid')