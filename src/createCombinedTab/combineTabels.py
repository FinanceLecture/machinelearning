# ---------------------
import math
import os
import re
from functools import partial

import numpy as np

from src.Exceptions.ExceptionHandler import invalid_calculation
from src.createCombinedTab.calculation import Calculation
from src.createCombinedTab.customMappers import *
from src.createCombinedTab.stringCalculations import *
# ---------------------

__file_ratings__ = 'ratingsByTic.csv'
__file_selected_economic_data__= 'selectedParameters.csv'
__file_economic_fundamentals__= 'allFundamentals.csv'
__file_economic_ratios__= 'allFinancialRatios.csv'
__file_selected_colums = 'selectedColumns'
file_ratings_quaterly__ = 'quaterlyRatings.csv'
relative_path_rating = os.path.realpath('../resources/ratings')
relative_path_economic_data = os.path.realpath('../resources/economicData')
relative_path_combined = os.path.realpath('../resources/combinedTable')


def merge_tables(execute = True):

    if not execute: return

    # The monthly ratings and the quaterly fundamental data are read from the local files
    # The whole database file of ratings and fundamental data has been downloaded for simplicity and performance reasons
    # data can be found under this link: https://wrds-web.wharton.upenn.edu/wrds/query_forms/navigation.cfm?navId=83
    df_ratings = pd.DataFrame(pd.read_csv(os.path.join(relative_path_rating, __file_ratings__), sep=',', error_bad_lines=False))
    df_economic_fundamental_data = pd.DataFrame(pd.read_csv(os.path.join(relative_path_economic_data, __file_economic_fundamentals__)
                                                            , sep=',', error_bad_lines=False, dtype='unicode'))
    # The monthly financial ratio data is also read from a local file
    df_economic_ratios_data = pd.DataFrame(pd.read_csv(os.path.join(relative_path_economic_data, __file_economic_ratios__)
                    , sep=',', error_bad_lines=False, dtype='unicode'))

    # the date is formatted and its column is renamed to date
    df_economic_fundamental_data = format_date_fundamentals(df_economic_fundamental_data)
    # the date is formatted and its column is renamed to date
    # Additionally the ratios are transformed from monthly to quaterly
    df_economic_ratios_data = format_date_ratios(df_economic_ratios_data)

    # read all calculations and column names which have been selected
    # to be in the final dataframe for the evaluation by the different models
    evaluated_columns = read_columns()
    # selects the calculations
    selected_colums = evaluated_columns[1]

    # sorts columns according to their dataframe (fundamental, financial ratio)
    tuple_selected = sort_columns(df_economic_fundamental_data, selected_colums)

    # merges the tables together
    result = merge_tables_correctly(tuple_selected, df_ratings, df_economic_fundamental_data, df_economic_ratios_data)

    # execute calculations and store the ratings in column catings
    result = formatting_table(result, evaluated_columns[0])

    # write the result into the file quaterlyRatings.csv
    result.to_csv(os.path.join(relative_path_combined, file_ratings_quaterly__), index=False)



def formatting_table(table, calculations):
    # in this function we execute the calculations the user put in

    columns_to_delete=[]

    for calculation in calculations:
        # we get the function and the two parameters of the calculation object
        function = calculation.get_action()
        x= table[calculation.get_first_parameter()].astype(float)
        y= table[calculation.get_second_parameter()].astype(float)

        # then receive the result and store it in the column with the result name
        # which we also get from the calculation object
        table[calculation.get_result()] = function(x=x,y=y)

        # afterwards all columns which were used for calculations
        for para in calculation.get_parameters():
            if not para in columns_to_delete:
                columns_to_delete.append(para)

    table=table.drop(columns=columns_to_delete)

    # retransform the ratings into their original form (AAA, BBB etc.) and store it in a column named ratings
    table['splticrm'] = table['splticrm'].apply(lambda x: get_rating_from_mean(x))
    table = table.rename(columns={'splticrm': 'ratings'})
    return table


def read_parameters(parameters):
    # initializes array which will hold all parameters of this calculation
    list_parameters = []
    # calculation will store the function which needs to be executed according to the user input
    # to do that it uses the package functools.
    # division is set as initial function
    calculation=partial(division)

    # depending on the input addition, division, multiplication or substraction is set
    # nested calculations are not possible. We tried to implement it with the help of recursion but were unsuccessful
    if "+" in parameters:
        list_parameters = parameters.split("+")
        calculation=partial(addition)

    elif "-" in parameters:
        list_parameters = parameters.split("-")
        calculation = partial(substraction)

    elif "/" in parameters:
        list_parameters = parameters.split("/")
        calculation = partial(division)

    elif "*" in parameters:
        list_parameters = parameters.split("*")
        calculation = partial(multiplication)

    # checks if the simple calculation only has two input parameters
    length = len(list_parameters)
    # a custom error is shown if wrong number of parameters were written to the file
    if length == 0 | length >2:
        invalid_calculation()

    # checks if all parameters only consist alphabetical and underscores
    for para in list_parameters:
        # checks if only alphabetical characters and/or underscores are in the parameter and
        # throws a custom error message if condition is not met
        if not re.match(r'^[A-Za-z_]+$', para):
            invalid_calculation(list_parameters[0], list_parameters[1])
            break

    # returns the parameters as array and the calculation as function
    return (list_parameters, calculation)

def read_columns():
    # array of calculations
    calculations = []

    # array of selected columns
    selected_colums = []

    with open(os.path.join(relative_path_economic_data, __file_selected_colums)) as f:
        # reads line by line of the file selected_columns in economic data
        # to get all calculations and columns which have been selected by the user
        while True:
            line = f.readline()

            # if line is empty it stops reading
            # ATTENTION when the last line has an empty string it is not be seen as empty
            # and it leads to an error later on in the process
            if not line:
                break

            # to remove the line break
            line = line.rstrip()

            # checks if the line includes a calculation
            if "=" in line:
                # replace all unnecessary blank spaces
                line = line.replace(" ", "")

                # split the line into result and calculation
                lines = line.split("=")

                # determines the parameters and the calculation to be made
                parameters_and_calculation = read_parameters(lines[1])

                # creates a calculation object
                c = Calculation(parameters_and_calculation[0], parameters_and_calculation[1], lines[0])

                # appends the object to the array of calculations
                calculations.append(c)

                # if a parameter is already listed as column itself then the parameter will not be added a second time
                # else it is added to later execute the calculation
                for parameter in parameters_and_calculation[0]:
                    selected_colums=check_if_in_list(selected_colums, parameter)
            else:
                # checks if the value is not added a second time
                selected_colums=check_if_in_list(selected_colums, line)

    return(calculations, selected_colums)


def check_if_in_list(list, value):
    # if a value already exists in a list it won't be added otherwise it will
    if not value in list:
        list.append(value)
    return list

def format_date_fundamentals(df):
    # converts the datadate to type datetime with the right
    # and stores it in a new column date
    df['date']= pd.to_datetime(df['datadate'], format='%Y%m%d')
    df = df.drop(columns=['datadate'])
    return df

def mapper_function(key):
    # transforms a rating string into a number according to the mapper function
    number = rating_to_nr.get(key)
    if number is None:
        #print("Couldn't find this key " + str(key))
        return None
    else:
        return number


def getting_quater_ratings(df_ratings):
    # convert credit rating to numeric value (only for purpose of creating quaterly data)
    # factorize was not possible to use because the rating needs to have a number according to its rating and
    # not according to when it appears in the dataframe
    df_ratings["splticrm"] = df_ratings["splticrm"].apply(lambda x: mapper_function(x))
    # convert "datadate" entry to format readable for DatetimeIndex function
    df_ratings = format_date_fundamentals(df_ratings)
    # set "datadate" as new index
    df_ratings.set_index('date', inplace=True)
    # group by (mainly) by tic and take the mean of every three months and create a business quater entry
    return (df_ratings.groupby(['tic', 'cusip', 'state', pd.Grouper(freq='BQ')])['splticrm'].mean())

def merge_tables_correctly(tuple_selected,df_ratings, df_economic_fundamental_data, df_economic_ratios_data):
    # since the ratings are registered in the database they have to be
    # transformed into a quaterly setup
    df_ratings = getting_quater_ratings(df_ratings)

    # initialize an empty dataframe
    result = pd.DataFrame()

    # if there is more than one selected column from the fundamental dataframe then
    # the quaterly ratings and the selected columns of the funamental data are merged
    # we decided to do a left outer join because a rating might not have an entry in fundamental data
    # but the result of this merge might be merged with financial ratios which could have entries for this rating
    # in the end information would be lost if no left outer join is done
    if (len(tuple_selected[0]) > 1):
        result = pd.merge(df_ratings,
                          df_economic_fundamental_data[tuple_selected[0]],
                          on=['tic', 'date'],
                          how='left',
                          left_index=False,
                          right_index=False)
    # if there is more than one selected column from the fundamental dataframe and
    # the result still is equal to the initialized dataframe than
    # a merge between ratings and financial ratios is performed
    # to be consistent we decided to use a left outer join as well even if a inner join would work the same
    # with a left join those ratings which don't have a machting gvkey in financial ratios will be
    # kept in the final table
    if len(tuple_selected[1]) > 0 & result.empty:
        result = pd.merge(result,
                          df_economic_ratios_data[tuple_selected[1]],
                          how='left',
                          right_on=['gvkey', 'date'],
                          left_on=['gvkey', 'date'],
                          left_index=False,
                          right_index=False)
    # if the first join of fundamental data and ratings occurred and
    # there is more than one column selected from the financial ratios dataframe then
    # this join is performed which is a left join but could also be a inner join like in the if statement before
    elif len(tuple_selected[1]) > 0 and not result.empty :
        result = pd.merge(df_ratings,
                          df_economic_ratios_data[tuple_selected[1]],
                          how='left',
                          right_on=['gvkey', 'date'],
                          left_on=['gvkey', 'date'],
                          left_index=False,
                          right_index=False)
    return result


def sort_columns(df_fundamentals, selected_columns):
    # this checks if the selected columns are from both dataframes
    # the reason for this is, that we can avoid an unnecessary merge later if all columns are from one dataframe

    # selected columns from the fundamental dataframe
    selected_columns_fundamentals = []
    # selected columns from the financial ratio dataframe
    selected_columns_ratios = []

    # for every column it checks to which data frame it belongs
    for column in selected_columns:
        if column in df_fundamentals.columns.values:
            selected_columns_fundamentals.append(column)
        else:
            selected_columns_ratios.append(column)

    # the gvkey must be selected in the fundamental dataframe otherwise a merge with the
    # financial ratio datafram is not possible
    selected_columns_fundamentals.append('gvkey')

    return (selected_columns_fundamentals, selected_columns_ratios)

def get_rating_from_mean(mean):
    # transforms the mean of the three months back to a non numeric rating
    if(math.isnan(mean)):
        return None
    rating = nr_to_rating.get(round(mean))
    if rating is None:
        print("something went wrong with key:"+ str(mean))
        return None
    else:
        return rating


def format_ratio_data_type(df):
    # remove the percentage sign in the column DIVYIELD so it can be converted to float later
    df['DIVYIELD'] = df['DIVYIELD'].str.rstrip('%').astype(float)
    # convert all types to floats (which is known) except the primary key (gvkey) and the date
    for column in df.columns.values:
        if column != 'date' and column !='gvkey':
            df[column] = df[column].astype(float)
    return df

def format_date_ratios(df):
    # converts public_date into a datetime object with the right format
    # assuming that it represents the same date where the data has be published and therefore
    # has the same meaning as datadate in the fundamental dataframe.
    # all other date alike columns are dropped
    df['date'] = pd.to_datetime(df['public_date'], format='%Y%m%d')
    df = df.drop(columns=['adate', 'qdate', 'public_date'])

    # convert all columns to float data types so that calculations later are possible
    # it is known that all ratios are numbers and therefore can be converted to float
    df = format_ratio_data_type(df)

    # set date as index otherwise groupby won't work
    df.set_index('date', inplace=True)

    # the data is grouped by company key (gvkey) and then
    # the three month mean has to be calculated to transform the data into quaterly data
    # The transformation has to be done because of the task description and
    # because the fundamental data is per business quater
    grouped =  df.groupby(['gvkey', pd.Grouper(freq='BQ')])
    df = grouped.aggregate(np.mean)
    return df

