class Calculation:
    def __init__(self, parameters, action, result):
        self.__parameters = parameters
        self.__action = action
        self.__result = result

    def set_result(self, result):
        self.__result = result

    def get_result(self):
        return self.__result

    def add_parameter(self, parameter):
        if not len(self.__parameters)>= 2:
            self.__parameters.append(parameter)

    def set_action(self, action):
        self.__action=action

    def get_parameters(self):
        return self.__parameters

    def get_action(self):
        return self.__action

    def get_first_parameter(self):
        return self.__parameters[0]

    def get_second_parameter(self):
        return self.__parameters[1]