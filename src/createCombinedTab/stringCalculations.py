import pandas as pd

def multiplication(x,y):
    return x*y

def addition(x,y):
    return x+y

def substraction(x,y):
    return x-y

def division(x,y):
    return round(x/y,2)

# prof for null/nan values
#    test = pd.DataFrame({'col1': [1, 2], 'col2': [3, np.nan]})
#    test = test["col1"]/test["col2"]
#    print(test)