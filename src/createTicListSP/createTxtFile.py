#---------------------
# Imports of panda, numpy and matplotlib package
# What to do if program doesn't recognize one or more packages
# --> Intall the missing packages.
# --> To do so hover over the missing package and press the short link to install
# --> or use the short cut alt, Umschalt, Enter while having the cursor on the missing package
import pandas as pd
import os
#---------------------

#define the filename
__fileListOfCompanies__ = 'SP500_CompanyList.csv'
__fileListOfTickers__ = 'tickerList'
__fileRatings__='ratingsByTic.csv'
__fileListOfGvKey='gvkeyList'
relative_path = os.path.realpath('../../resources/createTicListS&PResources')
relative_path_ratings = os.path.realpath('../../resources/ratings')
df = pd.read_csv(os.path.join(relative_path, __fileListOfCompanies__), sep=',')

with open(os.path.join(relative_path, __fileListOfTickers__),"w+") as f:
    f.writelines("%s\n" % tic for tic in list(df['TICKER']))

df_rating = pd.read_csv(os.path.join(relative_path_ratings,__fileRatings__), sep=',')

with open(os.path.join(relative_path, __fileListOfGvKey),"w+") as f:
    f.writelines("%s\n" % str(gvkey).zfill(6) for gvkey in list(df_rating['gvkey']))